package com.pacificspirit.todo_jersey.webapp.service;


/**
 * storage of todos.
 *
 * @author Hasan Diwan <hasandiwan@gmail.com>
 */
public abstract class StorageService implements StorageServiceIntf {

	protected String IDRegexp = "";

	/**
	 * compares done fields and calls MessageService if value has gone from false to true
	 * @param oldDone
	 * @param newDone
	 */
	public static void compareDone(boolean oldDone, boolean newDone, String title) {
		if (!oldDone && newDone) {
			try {
				MessageService.send("\"" + title + "\" task has been marked as done.");
			} catch (Exception e) {

			}
		}
	}

	@Override
	public String getIDRegexp() {
		return IDRegexp;
	}

}
