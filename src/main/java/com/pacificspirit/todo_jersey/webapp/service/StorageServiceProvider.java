package com.pacificspirit.todo_jersey.webapp.service;
import org.apache.log4j.Logger;

/**
 * Simple storage of todos.
 *
 * @author Hasan Diwan <hasandiwan@gmail.com>
 */
public class StorageServiceProvider{
    private static Logger logger = Logger.getLogger("com.pacificspirit.todo_jersey.webapp.service.StorageServiceProvider");
    private static StorageService s = null;
    public static StorageService get() {
	logger.info("StorageService get");
	if(s != null) {
	    return s;
	}
	String storageServiceType = System.getenv("TODO_JERSEY_STORAGE_TYPE");
	storageServiceType = (storageServiceType == null || storageServiceType.length() == 0) ? "InMemory" : storageServiceType;
	System.out.println(storageServiceType);
	    
	if(storageServiceType.contains("mongo")) {
	    s = new StorageServiceMongo();
	} else {
	    s = new StorageServiceInMemory();
	}
	return s;
    }
	
    public static String getIDRegexp() {
	return (s == null ? "" : s.getIDRegexp());
    }
     
}
