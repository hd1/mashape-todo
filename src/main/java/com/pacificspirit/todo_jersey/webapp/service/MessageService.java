package com.pacificspirit.todo_jersey.webapp.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.factory.MessageFactory;

public class MessageService {
	private static Logger logger = Logger.getLogger("com.pacificspirit.todo_jersey.webapp.service.MessageService");
	// TODO move to JVM System Property
	public static final String ACCOUNT_SID = "AC416522b02cbb2bdc9092c254a1e0d1a6";
	public static final String AUTH_TOKEN = "2ed90d4c6d769774e09115f3ab1c0ad2";

	public static final TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
	public static MessageFactory messageFactory;

	public static void init(MessageFactory mf) {
		messageFactory = mf;
	}

	public static void send(String msg) {
		if (messageFactory == null) {
			messageFactory = client.getAccount().getMessageFactory();
		}
		try {

			// Build the parameters
			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("From", "+17786542410"));
			params.add(new BasicNameValuePair("To", "+16047907978"));
			params.add(new BasicNameValuePair("Body", msg));

			messageFactory.create(params);
		} catch (Exception e) {
			logger.fatal(String.format("Exception of type \"%s\" occurred in MessageService#send", e.getMessage()));
		}
	}
}
