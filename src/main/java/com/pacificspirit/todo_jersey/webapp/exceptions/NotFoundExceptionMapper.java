package com.pacificspirit.todo_jersey.webapp.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * An exception mapper to return 404 responses when a {@link CustomNotFoundException} is thrown.
 *
 * @author Hasan Diwan <hasandiwan@gmail.com>
 */
@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<CustomNotFoundException> {
	private Exception exception;

	@Override
	public Response toResponse(CustomNotFoundException anException) {
		this.setException(anException);
		return Response.status(404).build();
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception anException) {
		this.exception = anException;
	}

}
